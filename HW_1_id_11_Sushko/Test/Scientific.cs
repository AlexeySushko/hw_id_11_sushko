﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Test
{
    public class Scientific : Calculator
    {
        Calculator sc = new Calculator();

        public double percentMethod(double numb, double perc)
        {
            return sc.multiplyVoid(sc.separationVoid(numb, 100), perc);
        }



        public double squareRoot(double numb)
        {
            return Math.Sqrt(numb);
        }



        public double sumArray(double[] arr)
        {
            double res = 0;
            foreach (double element in arr)
            {
                res = sc.sumVoid(res, element);
            }
            return res;
        }


    }
}

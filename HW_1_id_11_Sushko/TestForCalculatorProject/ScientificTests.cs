﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    class ScientificTests
    {
        
        Scientific sc = new Scientific();

        [Test]
        public void PercentTest()
        {
            Assert.AreEqual(12.2, sc.percentMethod(100, 12.2), "12 percent of 100 is equal to 12");
        }


        [Test]
        public void SquareRootTest()
        {
            Assert.AreEqual(3, sc.squareRoot(9), "The square root of 9 is equal to 3");
        }


        [Test]
        public void SumArrayTest()
        {
            double[] arr = new double[] { 1.2, 1.3, 10 };
            Assert.AreEqual(12.5, sc.sumArray(arr), "1.2 + 1.3 + 10 = 12.5");
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    class Test
    {
        Calculator calc = new Calculator();

        [Test]
        public void TestSum()
        {
            Assert.AreEqual(8, calc.sumVoid(3, 5), "3+5=8");
        }

        [Test]
        public void TestSumNegative()
        {

            Assert.AreNotSame(4, calc.sumVoid(3, 3), "3+3 != 4");
        }

       [Test]
        public void TestMinus()
        {
            Assert.NotZero(calc.minusVoid(7, 5), "7-5=2");
        }

        [Test]
        public void TestMinusNegative()
        {
            Assert.AreNotEqual(2, calc.minusVoid(7, 3), "7-3 != 2");
        }

        [Test]
        public void TestMultiply()
        {
            Assert.AreEqual(6, calc.multiplyVoid(2, 3), "2*3=6");
        }

        [Test]
        public void TestSeparation()
        {
            Assert.AreEqual(5, calc.separationVoid(10, 2), "10/2=5");
        }
    }
}

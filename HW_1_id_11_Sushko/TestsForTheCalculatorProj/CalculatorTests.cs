﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    class Test
    {
        Calculator calc = new Calculator();

        [Test]
        public void TestSum()
        {
            Assert.AreEqual(calc.sumVoid(3, 5), 8, "3+5=8");
        }

        [Test]
        public void TestSumNegative()
        {
            Assert.AreNotEqual(calc.sumVoid(3, 3), 4, "3+3 != 4");
        }

        [Test]
        public void TestMinus()
        {
            Assert.AreEqual(calc.minusVoid(7, 5), 2, "7-5=2");
        }

        [Test]
        public void TestMinusNegative()
        {
            Assert.AreNotEqual(calc.minusVoid(7, 3), 2, "7-3 != 2");
        }

        [Test]
        public void TestMultiply()
        {
            Assert.AreEqual(calc.multiplyVoid(2, 3), 6, "2*3=6");
        }

        [Test]
        public void TestSeparation()
        {
            Assert.AreEqual(calc.separationVoid(10, 2), 5, "10/2=5");
        }
    }
}
